import ApcUpsData from "./ApcUpsData"


function extractFloat(value: string) {
    const[float] = value.split(' ')
    return parseFloat(float)
}

export default class Parser {

    public static parseLine(line: string) {
        line = line.trim()

        const regex = /^([\w\s]+):(.*)$/gm
        const res = regex.exec(line)
        if(res === null) {
            return {name: null, value: null}
        }
        let name:string = res[1].trim()
        let value:any = res[2].trim()

        return {name, value}
    }

    public static parseString(string: string) {
        const lines = string.split('\n')
        const data: any = {}
        for(const line of lines) {
            const {name, value} = Parser.parseLine(line)
            if(name !== null) {
                // TODO: Handle null
                data[name] = value
            }
        }

        return new ApcUpsData(
            data['APC'],
            new Date(data['DATE']),
            data['HOSTNAME'],
            data['VERSION'],
            data['UPSNAME'],
            data['CABLE'],
            data['DRIVER'],
            data['UPSMODE'],
            new Date(data['STARTTIME']),
            data['MODEL'],
            data['STATUS'],
            extractFloat(data['LINEV']),
            extractFloat(data['LOADPCT']),
            extractFloat(data['BCHARGE']),
            data['TIMELEFT'],
            data['MBATTCHG'],
            data['MINTIMEL'],
            data['MAXTIME'],
            extractFloat(data['OUTPUTV']),
            data['SENSE'],
            data['DWAKE'],
            data['DSHUTD'],
            extractFloat(data['LOTRANS']),
            extractFloat(data['HITRANS']),
            extractFloat(data['RETPCT']),
            extractFloat(data['ITEMP']),
            data['ALARMDEL'],
            extractFloat(data['BATTV']),
            extractFloat(data['LINEFREQ']),
            data['LASTXFER'],
            data['NUMXFERS'],
            new Date(data['XONBATT']),
            data['TONBATT'],
            data['CUMONBATT'],
            new Date(data['XOFFBATT']),
            new Date(data['LASTSTEST']),
            data['SELFTEST'],
            data['STESTI'],
            data['STATFLAG'],
            new Date(data['MANDATE']),
            data['SERIALNO'],
            new Date(data['BATTDATE']),
            extractFloat(data['NOMOUTV']),
            extractFloat(data['NOMINV']),
            extractFloat(data['NOMBATTV']),
            extractFloat(data['NOMPOWER']),
            data['FIRMWARE'],
            new Date(data['END APC']),            
        )
    }
}