export default class ApcUpsData {
    public apc: string;
    public date: Date;
    public hostname: string;
    public version: string;
    public name: string;
    public cable: string;
    public driver: string;
    public upsMode: string;
    public startTIme: Date;
    public model: string;
    public status: string;
    /**
     * Voltage
     */
    public lineVoltage: number;
    public loadPercentage: number;
    public batteryCharge: number;
    public timeLeft: string;
    public mBatteryCharge: number;
    public minTimeLeft: string;
    public maxTime: string;
    public outputVoltage: number;
    public sense: string;
    public dWake: string;
    public dShutdown: string;
    public lowTrans: number;
    public highTrans: number;
    public iTemperature: number;
    public alarmDelay: string;
    public batteryVoltage: number;
    public lineFrequency: number;
    public lastXfer: string;
    public numXfer: string;
    public xOnBattery: Date;
    public tOnBattery: string;
    public cumOnBattery: string;
    public xOffBattery: Date;
    public lastSelftest: Date;
    public selftest: string;
    public sTestI: string;
    public statFlag: string;
    public manDate: Date;
    public serialNumber: string;
    public batteryDate: Date;
    public retPercentage: number;
    public nomOutVoltage: number;
    public nomInVoltage: number;
    public nomBatteryVoltage: number;
    public nomPower: number;
    public firmware: string;
    public endApc: Date;
    

    constructor(
        apc: string, 
        date: Date,
        hostname: string,
        version: string,
        name: string,
        cable: string,
        driver: string,
        upsMode: string,
        startTime: Date,
        model: string,
        status: string,
        lineVoltage: number,
        loadPercentage: number,
        batteryCharge: number,
        timeLeft: string,
        mBatteryCharge: number,
        minTimeLeft: string,
        maxTime: string,
        outputVoltage: number,
        sense: string,
        dWake: string,
        dShutdown: string,
        lowTrans: number,
        highTrans: number,
        retPercentage: number,
        iTemperature: number,
        alarmDelay: string,
        batteryVoltage: number,
        lineFrequency: number,
        lastXfer: string,
        numXfer: string,
        xOnBattery: Date,
        tOnBattery: string,
        cumOnBattery: string,
        xOffBattery: Date,
        lastSelftest: Date,
        selftest: string,
        sTestI: string,
        statFlag: string,
        manDate: Date,
        serialNumber: string,
        batteryDate: Date,
        nomOutVoltage: number,
        nomInVoltage: number,
        nomBatteryVoltage: number,
        nomPower: number,
        firmware: string,
        endApc: Date,
        
    ) {
        this.apc = apc
        this.date = date
        this.hostname = hostname
        this.version = version
        this.name = name
        this.cable = cable
        this.driver = driver
        this.upsMode = upsMode
        this.startTIme = startTime
        this.model = model
        this.status = status
        this.lineVoltage = lineVoltage
        this.loadPercentage = loadPercentage
        this.batteryCharge = batteryCharge
        this.timeLeft = timeLeft
        this.mBatteryCharge = mBatteryCharge
        this.minTimeLeft = minTimeLeft
        this.maxTime = maxTime
        this.outputVoltage = outputVoltage
        this.sense = sense
        this.dWake = dWake
        this.dShutdown = dShutdown
        this.lowTrans = lowTrans
        this.highTrans = highTrans
        this.iTemperature = iTemperature
        this.alarmDelay = alarmDelay
        this.batteryVoltage = batteryVoltage
        this.lineFrequency = lineFrequency
        this.lastXfer = lastXfer
        this.numXfer = numXfer
        this.xOnBattery = xOnBattery
        this.tOnBattery = tOnBattery
        this.cumOnBattery = cumOnBattery
        this.xOffBattery = xOffBattery
        this.lastSelftest = lastSelftest
        this.selftest = selftest
        this.sTestI = sTestI
        this.statFlag = statFlag
        this.manDate = manDate
        this.serialNumber = serialNumber
        this.batteryDate = batteryDate
        this.retPercentage = retPercentage
        this.nomOutVoltage = nomOutVoltage
        this.nomInVoltage = nomInVoltage
        this.nomBatteryVoltage = nomBatteryVoltage
        this.nomPower = nomPower
        this.firmware = firmware
        this.endApc = endApc        
    }
}