
import {exec} from 'child_process'
import Parser from './parser'

const asyncExec = (command: string): Promise<any> => {
    return new Promise((resolve, reject) => {
        exec('sh hi.sh', (error, stdout, stderr) => {
            if (error !== null) {
                reject(error)
            }
            resolve({stdout, stderr})
        });
    })
}

export default class ApcUps {
    public async data() {
        const {stdout} = await asyncExec('apcaccess status')
        return Parser.parseString(stdout)
    }
}